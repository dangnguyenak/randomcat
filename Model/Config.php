<?php

namespace Advox\RandomCat\Model;

class Config
{
    const URL = 'https://cataas.com/cat';
    const URL_WITH_TEXT = 'https://cataas.com/cat/says/';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    public function getRandomCatUrl($parameter = '')
    {
        $text = $this->getText();
        if (empty($text)) {
            return self::URL . '?' . $parameter;
        }
        return self::URL_WITH_TEXT . $text . '?' . $parameter;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return (string)$this->scopeConfig->getValue('catalog/frontend/text');
    }

    /**
     * @return bool
     */
    public function isEnabledOnCategoryPage()
    {
        return (bool)$this->scopeConfig->getValue('catalog/frontend/enable_random_cat_category_page');
    }

    /**
     * @return bool
     */
    public function isEnabledOnProductPage()
    {
        return (bool)$this->scopeConfig->getValue('catalog/frontend/enable_random_cat_product_page');
    }
}
