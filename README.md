Module that shows random cat on category page and product page
- Can enable/disable showing on category page and product page, setting in Stores > Configuration > Catalog > Storefront > 
  - Enable Random Cat On Category Page
  - Enable Random Cat On Product Page
  - Text Shows On Random Cat Picture
- Configure **Text Shows On Random Cat Picture** will show text with cat picture, if leave empty, it only shows cat picture
- It only shows random for the first time before page cached, if the page is cached, it won't randomize picture again 
