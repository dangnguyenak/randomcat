<?php

namespace Advox\RandomCat\Plugin;

use Magento\Catalog\Block\Product\View\Gallery;
use Magento\Framework\Data\Collection;

class GalleryPlugin
{
    /**
     * @var \Advox\RandomCat\Model\Config
     */
    private $config;

    /**
     * @param \Advox\RandomCat\Model\Config $config
     */
    public function __construct(
        \Advox\RandomCat\Model\Config $config
    ) {
        $this->config = $config;
    }

    /**
     * @param Gallery $subject
     * @param Collection $result
     * @return Collection
     */
    public function afterGetGalleryImages(Gallery $subject, Collection $result): Collection
    {
        if (!$this->config->isEnabledOnProductPage() || !$result instanceof \Magento\Framework\Data\Collection || empty($result)) {
            return $result;
        }
        foreach ($result as $image) {
            $galleryImagesConfig = $subject->getGalleryImagesConfig()->getItems();
            foreach ($galleryImagesConfig as $imageConfig) {
                $image->setData(
                    $imageConfig->getData('data_object_key'),
                    $this->config->getRandomCatUrl($image->getId())
                );
            }
        }
        return $result;
    }
}
