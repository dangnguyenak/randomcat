<?php

namespace Advox\RandomCat\Block\Product;

class Image extends \Magento\Catalog\Block\Product\Image
{
    /**
     * @var \Advox\RandomCat\Model\Config
     */
    private $config;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Advox\RandomCat\Model\Config $config
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Advox\RandomCat\Model\Config                    $config,
        array                                            $data = []
    ) {
        parent::__construct($context, $data);
        $this->config = $config;
    }

    /**
     * @return string
     */
    public function getImageUrl(): string
    {
        if (!$this->config->isEnabledOnCategoryPage()) {
            return parent::getImageUrl();
        }
        return $this->config->getRandomCatUrl(uniqid());
    }
}
